# modalhacks_cloudfunctions
modal.systems is a mobile SDK / App that helps to trigger a ride request at the perfect moment in time when our AI tells the user has based on his parameters the smallest waiting time for the driver.

To successfully build and run follow the instructions: 

## Develop locally

- Make sure that you have npm and node.js installed on your machine
- Cloud Functions runs Node v.6.11.5, once you have the above two run the following code:
`npm install -g firebase-tools`

Further instructions can be found here: [Firebase Cloud Function Tutorial](https://firebase.google.com/docs/functions/get-started)


## What to build next?
Since our team was among the smallest team's participating, we couldn't build all desired components of the systems we wanted. What we would have built next if more time available, would be available:

- This would have been our watcher onto the changes of a users location and once he a transportation obect has changed.
- Also a listener onto a users object from here.

