// Imports
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const request = require('request');
admin.initializeApp(functions.config().firebase);

// Variables
const db = admin.database();

exports.startAlertWatcher = functions.database.ref("transportPipeline")
    .onUpdate(event => {
    startWatcher();
});

function startWatcher() {
  const alertRef = db.ref('transportPipeline');


// TODO Just started with this from an old sample

  alertRef.once('value', function(snapshot){
    var arrayOfAllTransportationWatchers = snapshot.val();
    console.log(snapshot.val());

    // 1. Iterate over all watchers and query price for each if user is in geo fence
    for (var cryptoShort in arrayOfAllTransportationWatchers) {
      getTheCryptoPrice(cryptoShort, function(cryptoShort, priceUSD) {
        console.log('Retrieved price for ' + cryptoShort + ' ' + priceUSD);
        var priceAs100000 = priceUSD * 100000;

        // 2. Iterate over buckets and inside buckets over very user and check if the
        //    retrieved price is < / > than the signed up price
        var allBucketsOfAlert = arrayOfAllTransportationWatchers[cryptoShort];
        for (var bucket in allBucketsOfAlert) {

          for(var userID in allBucketsOfAlert[bucket]) {
            var signedUpPrice = allBucketsOfAlert[bucket][userID];

            // 3. decide(userNode, currentBucket, queryPrice) for ever user if we should sent a push
            //    and delete the user from the alerts endpoint + in his userprofile
            var shouldPush = decide(userID, bucket, signedUpPrice, priceAs100000);
            if (shouldPush) {
              sendPushNotificationAboutAlert(userID, cryptoShort, bucket);
              deleteFromAlertsNode(cryptoShort, bucket, userID);
              deleteFromUserProfil(userID, cryptoShort, bucket);
            }
          }
        }
      })
    }
  });
}

// Wait and sleep for 1 minute
setInterval(function(){ restartTheWatchFunction(); }, 59000);

function getTheCryptoPrice(cryptoShort, continueWith) {
  var options = {
    uri: "https://min-api.cryptocompare.com/data/price?fsym=" + cryptoShort + "&tsyms=USD",
    method: 'GET',
    headers: {
      "Accept-Language": "en_US",
      "Content-Type": "application/json"
    }
  };
  request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      continueWith(cryptoShort, JSON.parse(body).USD);
    } else {
      console.log(error);
    }
  });
}
